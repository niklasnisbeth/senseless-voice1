Senseless: A highly illogical synthesizer
====

![3d view](img/glamour-render.png)

A few years ago, when I was getting into electronics, I pulled a large amount of 74HC logic ICs and some DACs and ADCs from some discarded medical equipment. This was around the same time as acreil's legendary (to me, anyway) post "A taxonomy of early digital synthesizers" on Gearslutz which made me think a lot about how samplers and digital synths worked. I had an Akai S900 at the time and was particularly fascinated with polyphonic variable rate playback. 

The two things sort of came together and I've been sketching designs for audio samplers and wavetable synthesizers in an 80's style on and off, but never taken it further because it's just so dumb compared to a modern solution based on a microcontroller or an FPGA. 

But I haven't been able to shake my fascination with the look of these rows-of-DIP type circuits, nor the limitations of designing with (mostly) chips I have on hand. And then some guy on the internet planted the phrased "follow your dreams, even when your dreams are stupid" in my head, and now I've drawn up the digital part of the simplest dual-voice board I could come up with and I think I might have to see if I can get it to actually make sound... and hear what that sound would be like. 

To finish it I need a "main" board with memory (that would be shared by up to probably four of these voice boards) and a microcontroller to generate some clocks and control signals. I can probably do this on veroboard to begin with. To be musically useful, it'd also need a filter/VCA analog board per voice.

![schematics](img/schematic-excerpt.png)

Sample-based synthesis basically consists of getting samples out of memory and putting them through a DAC at a sample rate. If you have a sound at one pitch and want to play it at another, you can either change the actual playback rate, or pretend that you do by resampling the signal. For this board I've done the simplest thing, so it's only one '590 8-bit counter and two '574 8-bit flip-flop registers which implement a very simple variable-sample-rate playback, the core of which is shown here. 

The counter is driven by a clock running at the sampling rate and handles the 8 lowest address bits. The first latch is for the 8 highest bits of the address. It is reloaded when the counter overflows, its inputs are connected to a shift register (not depicted). Basically, the circuit will generate 256 consecutive addresses ("a segment") and then reload the MSB latch with the "next segment". It is assumed that the controller will have updated the shift register at some point before 256 samples have been played if the segment is not intended to loop. A flip-flop (not shown) is set on counter overflow and reset by the controller after updating the shift register, so the voice circuit can tell the controller when it needs to shift out the address of the next segment. 

An additional 4 address bits are available (giving 20 in all), but they are not latched so playback of sounds longer than 64K samples is probably not practical. Also, the counter only goes forwards so we are limited to playing back entire 256-sample segments from start to end, and waveform data will have to be prepared accordingly. 

The second latch simply stores the audio data coming from the memory so that it can be loaded into the DAC later, in time with the sample clock.

![dense layout!](img/layout.png)

Having the counter and address latch significantly loosens the exactness of timing required for playback: for a 200 kHz sample rate, controller updates are needed not every 5 microseconds, but roughly every 1.3 millisecond if no segment loops. Also, since the counters and latch have tri-state outputs, we're also able to connect multiple boards like this to the same memory. The limit to the amount of voices will be a function of the sampling rates, the propagation delays of the logic, and the access time of the memory. 

This should support up to 8 voices at up to approximately 200 kHz sample rates. This is a little too slow for 256-sample wavetables (about an octave over concert pitch), so for true PPG wave style sounds we'd have to waste a little memory in the higher octaves, putting wavetables multiple times in a segment. 

The memory can be a single chip shared between voices running at different sample rates because we're latching the sample data locally, so we can update the DAC (which has its own internal latch) at a different rate than what we use to update the addresses. 

The DAC Is an 8-bit AD7524 general-purpose DAC. The analogue circuitry after it is straight from the datasheet to give a bipolar output. There's no filtering or amplitude control, so the signals from the DAC are connected to a header that we can stack a filter/VCA board on. 

This exact design has not been tested. I know some people like their huge breadboard setups, but I was never much of a fan. It was at the very least fun to take one of my sketches to completion, but I'm still not quite sure I'll get it fabbed.

![puns](img/back.png)

This design uses 15 chips for two channels of very limited audio playback. More features raise the chip count remarkably quickly. 74HC-series counters that can be preset are only four bits wide, so if you want to be able to start at an arbitrary point in a 20-bit memory, you need five of them. If you want play back to end at a specific point, you'll also need some latches and comparators. Plus control signals. If you don't want variable rate playback, you'll need some setup involving a bunch of adders and God knows what else. This will have to do for now!
